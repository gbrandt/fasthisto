#include <FastHisto.hpp>
int main(int argc, char *argv[]) {

  FastHisto::Histo2Int h("h2d_u8_100_100","test_hist","a","b");
  h.fill(102,102);
  h.fill(2,3,10);
  variant32 d;
  h.toVariant(d);
  d.dump();
  FastHisto::Histo2IntImpl<unsigned,100,100> h1("test_hist","a","b");
  h1.fill(1,1);
  h1.toVariant(d);
  d.dump();
  FastHisto::Histo2Int h2(h1);
  h2.toVariant(d);
  d.dump();
  std::cout << int(h(2,3)) << std::endl;
  return 0;

}
